package com.creo.gostories;

/**
 * Created by Admin_2 on 13-08-2015.
 */
public class Registration {
    private String emailID;
    private String mobileNumber;
    private String udidNumber;

    public String getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(String totalPoint) {
        this.totalPoint = totalPoint;
    }

    private String totalPoint;
    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    private String walletBalance;

    public String getErrorKey() {
        return errorKey;
    }

    public void setErrorKey(String errorKey) {
        this.errorKey = errorKey;
    }

    private String promoCode;
    private String error;
    private String errorKey;

    public String getWalletBalance() {
        return walletBalance;
    }

    public void setWalletBalance(String walletBalance) {
        this.walletBalance = walletBalance;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public void setPromoCode(String promoCode) {
        this.promoCode = promoCode;
    }

    public String getEmailID() {
        return emailID;
    }

    public void setEmailID(String emailID) {
        this.emailID = emailID;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getUdidNumber() {
        return udidNumber;
    }

    public void setUdidNumber(String udidNumber) {
        this.udidNumber = udidNumber;
    }




}
