package com.creo.gostories;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by Admin_2 on 28-07-2015.
 */
public class SplashActivity extends Activity {

    // Splash screen timer
    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        SharedPreferences pref = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
        final String restored = pref.getString("email", "");
        new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                if(restored.equals("")) {
                    Intent i = new Intent(SplashActivity.this, UserRegisterationActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }
                else{
                    Intent i = new Intent(SplashActivity.this, HomeActivity.class);
                    startActivity(i);
                    // close this activity
                    finish();
                }
            }
        }, SPLASH_TIME_OUT);
    }

}

