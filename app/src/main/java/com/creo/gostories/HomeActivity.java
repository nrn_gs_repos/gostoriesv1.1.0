package com.creo.gostories;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.GestureDetector.SimpleOnGestureListener;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;


public class HomeActivity extends Activity implements OnClickListener{

  //  private static final java.lang.String STATE_SCORE = "Home";
    private Button play,pause;
    static  AudioPlayerView audioPlayerViewObj;
    static final String LogTag = "GoStoriesLogTag";
    Button mainButton ;
    Button stopAudioBtn;
    Button playPauseAudioBtn;
    private static final int SWIPE_MIN_DISTANCE = 120;
    private static final int SWIPE_THRESHOLD_VELOCITY = 200;
    private ViewFlipper mViewFlipper;
    private AnimationListener mAnimationListener;
    private Context mContext;
    InterstitialAd mInterstitialAd;
    Button mNewGameButton;
    SharedPreferences pref;
//    private MediaPlayer mediaPlayer;
//    public TextView songName, duration;
//    private double timeElapsed = 0, finalTime = 0;
//    private int forwardTime = 2000, backwardTime = 2000;
//    private android.os.Handler durationHandler = new android.os.Handler();
//    private SeekBar seekbar;
//int mCurrentScore;

    @SuppressWarnings("deprecation")
    private final GestureDetector detector = new GestureDetector(new SwipeGestureDetector());

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.main);

        pref=getApplication().getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE);
        String email = pref.getString("email", "");
        String promo_code = pref.getString("promo_code", "");
        String toal_point = pref.getString("toal_point", "");

        Log.e(HomeActivity.LogTag, "email in home" + email);
        Log.e(HomeActivity.LogTag, "promo_code in home" + promo_code);
        Log.e(HomeActivity.LogTag, "toal_point in home" + toal_point);
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        mNewGameButton = (Button) findViewById(R.id.interst);
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");

        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                requestNewInterstitial();
                //beginPlayingGame();
            }
        });

        requestNewInterstitial();
        mNewGameButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mInterstitialAd.isLoaded()) {
                    mInterstitialAd.show();
                } else {
                    beginPlayingGame();
                }
            }
        });

        beginPlayingGame();

        audioPlayerViewObj = new AudioPlayerView(this);
        mainButton = (Button)findViewById(R.id.streamBtn);

        mainButton.setOnClickListener(this);
        mainButton.setVisibility(View.INVISIBLE);
        //audioPlayerViewObj.mainActivityObj = this;
        audioPlayerViewObj.audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBarId);
        audioPlayerViewObj.bufferTextView = (TextView) findViewById(R.id.bufferTextViewId);
        audioPlayerViewObj.bufferText = (TextView) findViewById(R.id.bufferTextView);
        audioPlayerViewObj.playTextView = (TextView) findViewById(R.id.playedTextViewId);
        audioPlayerViewObj.playText = (TextView) findViewById(R.id.playedTextView);
        audioPlayerViewObj.bufferTextView.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.bufferText.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.playTextView.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.playText.setVisibility(View.INVISIBLE);
        playPauseAudioBtn = (Button) findViewById(R.id.playAudioBtnId);
        audioPlayerViewObj.playOrPauseBtn = playPauseAudioBtn;
        audioPlayerViewObj.SongUrl = "http://gostories.co.in/Sandeep/prastawana.mp3";
        stopAudioBtn = (Button) findViewById(R.id.stopAudioBtnId);
        play = (Button) findViewById(R.id.play);
        pause =	(Button) findViewById(R.id.stop);

        stopAudioBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayerViewObj.stopAudioPlayer();

                }
            });

            playPauseAudioBtn.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(SelectedStoryActivity.audioPlayerViewObj != null){

                    Log.d("HomeActivityaudioPlayerViewObj.mp", String.valueOf(SelectedStoryActivity.audioPlayerViewObj.mp));
                    SelectedStoryActivity.audioPlayerViewObj.stopAudioPlayer();
                    Log.d("HomeActivityaudioPlayerViewreleseObj.mp", String.valueOf(SelectedStoryActivity.audioPlayerViewObj.mp));
                    audioPlayerViewObj.playOrPauseAudioPlayer();
                }
                else{

                    audioPlayerViewObj.playOrPauseAudioPlayer();
                }
            }
        });

        //initialize views
        // initializeViews();
        mContext = this;
        mViewFlipper = (ViewFlipper) this.findViewById(R.id.view_flipper);
        mViewFlipper.setAutoStart(true);

        mViewFlipper.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View view, final MotionEvent event) {
                detector.onTouchEvent(event);
                return true;
            }
        });

        findViewById(R.id.play).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //sets auto flipping
                mViewFlipper.setAutoStart(true);
                mViewFlipper.setFlipInterval(3000);
                mViewFlipper.startFlipping();
                pause.setVisibility(View.VISIBLE);
                play.setVisibility(View.INVISIBLE);

            }
        });

        findViewById(R.id.stop).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                //stop auto flipping
                mViewFlipper.stopFlipping();
                play.setVisibility(View.VISIBLE);
                pause.setVisibility(View.INVISIBLE);
            }
        });
        //animation listener
        mAnimationListener = new Animation.AnimationListener() {
            public void onAnimationStart(Animation animation) {
                //animation started event
            }
            public void onAnimationRepeat(Animation animation) {
            }
            public void onAnimationEnd(Animation animation) {
                //TODO animation stopped event
            }
        };
    }

    private void requestNewInterstitial() {
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("SEE_YOUR_LOGCAT_TO_GET_YOUR_DEVICE_ID")
                .build();

        mInterstitialAd.loadAd(adRequest);
    }

    private void beginPlayingGame() {
        // Play for a while, then display the New Game Button
    }

//    String sMyText = "some text";
//    int nMyInt = 10;

//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        // Save away the original text, so we still have it if the activity
//        // needs to be killed while paused.
//        outState.putString("my_text", sMyText);
//        outState.putInt("my_int", nMyInt);
//        Toast.makeText(this, "onSaveInstanceState()", Toast.LENGTH_LONG).show();
//        Log.i("onSaveInstanceState", "onSaveInstanceState()");
//    }
//
//    String sNewMyText = "";
//    int nNewMyInt = 0;
//
//    @Override
//    protected void onRestoreInstanceState(Bundle savedInstanceState) {
//        super.onRestoreInstanceState(savedInstanceState);
//        // restore saved values
//        sNewMyText = savedInstanceState.getString("my_text");
//        nNewMyInt = savedInstanceState.getInt("my_int");
//        Toast.makeText(this, "onRestoreInstanceState()", Toast.LENGTH_LONG).show();
//        Log.i("onRestoreInstanceState", "onRestoreInstanceState()");
//
//    }
//
//
//
//
//    void OnDestroy() { }

   /* @Override
    protected void onPause(){
        super.onPause();
        if (audioPlayerViewObj.mp != null) {
            audioPlayerViewObj.mp.pause();
            audioPlayerViewObj.playOrPauseBtn.setText(audioPlayerViewObj.playBtnTitle);
            audioPlayerViewObj.playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_play);

        }
    }

    @Override
    protected void onResume() {
        super.onResume();

            if (audioPlayerViewObj.mp == null) {

            }

           else{
            audioPlayerViewObj.mp.start();
                audioPlayerViewObj.playOrPauseBtn.setText(audioPlayerViewObj.pauseBtnTitle);
                audioPlayerViewObj.playOrPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);

            }


    }*/
    //    @Override
//    protected void onResume(){
//        super.onResume();
//        if (audioPlayerViewObj.mp == null) {
//            audioPlayerViewObj.mp.start();
//        }
//    }
    /*public void initializeViews(){
        songName = (TextView) findViewById(R.id.songName);
        mediaPlayer = MediaPlayer.create(this, R.raw.prastawana);
        finalTime = mediaPlayer.getDuration();
        duration = (TextView) findViewById(R.id.songDuration);
        seekbar = (SeekBar) findViewById(R.id.seekBar);
        songName.setText("");

        seekbar.setMax((int) finalTime);
        seekbar.setClickable(true);
    }


    // play mp3 song
    public void play(View view) {
        mediaPlayer.start();
        timeElapsed = mediaPlayer.getCurrentPosition();
        seekbar.setProgress((int) timeElapsed);
        durationHandler.postDelayed(updateSeekBarTime, 100);
    }

    //handler to change seekBarTime
    private Runnable updateSeekBarTime = new Runnable() {
        public void run() {
            //get current position
            timeElapsed = mediaPlayer.getCurrentPosition();
            //set seekbar progress
            seekbar.setProgress((int) timeElapsed);
            //set time remaing
            double timeRemaining = finalTime - timeElapsed;
            duration.setText(String.format("%d min, %d sec", TimeUnit.MILLISECONDS.toMinutes((long) timeElapsed), TimeUnit.MILLISECONDS.toSeconds((long) timeRemaining) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes((long) timeRemaining))));

            //repeat yourself that again in 100 miliseconds
            durationHandler.postDelayed(this, 100);
        }
    };

    // pause mp3 song
    public void pause(View view) {
        mediaPlayer.pause();
    }

    // go forward at forwardTime seconds
    public void forward(View view) {
        //check if we can go forward at forwardTime seconds before song endes
        if ((timeElapsed + forwardTime) <= finalTime) {
            timeElapsed = timeElapsed + forwardTime;

            //seek to the exact second of the track
            mediaPlayer.seekTo((int) timeElapsed);
        }
    }

    // go backwards at backwardTime seconds
    public void rewind(View view) {
        //check if we can go back at backwardTime seconds after song starts
        if ((timeElapsed - backwardTime) > 0) {
            timeElapsed = timeElapsed - backwardTime;

            //seek to the exact second of the track
            mediaPlayer.seekTo((int) timeElapsed);
        }
    }*/

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // toggle nav drawer on selecting action bar app icon/title

        // Handle action bar actions click
        switch (item.getItemId()) {
            case R.id.action_settings:
                recreate();

                return true;
            case R.id.action_settings1:
                Intent intent = new Intent(HomeActivity.this,StoryListActivity.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    class SwipeGestureDetector extends SimpleOnGestureListener {
        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
            try {
                // right to left swipe
                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_in));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext, R.anim.left_out));
                    // controlling animation
                    mViewFlipper.getInAnimation().setAnimationListener(mAnimationListener);
                    mViewFlipper.showNext();
                    return true;
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(mContext, R.anim.right_in));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(mContext,R.anim.right_out));
                    // controlling animation
                    mViewFlipper.getInAnimation().setAnimationListener(mAnimationListener);
                    mViewFlipper.showPrevious();
                    return true;
                }
            } catch (Exception error101) {
                error101.printStackTrace();
                Log.d(HomeActivity.LogTag,"\n  error101. : "+ error101);
            }
            return false;
        }
    }

    public void startAudioStreaming(View v){ audioPlayerViewObj.streamAudioPlayer();}

    @Override
    public void onClick(View v) {
        startAudioStreaming(v);
    }
}


