package com.creo.gostories;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Admin_2 on 28-07-2015.
 */
public class SelectedStoryActivity extends Activity implements View.OnClickListener {

    TextView storyname,bookname,duration,director,authorname,artistname;
    ImageView authoreimage,artistimage,authoreimageinfo,artistimageinfo;
    static AudioPlayerView audioPlayerViewObj = null;
    Button stopAudioBtn;
    Button playPauseAudioBtn,preview,story;
    String clickedId;
    String idType;
    final String preview_story = "preview_story";
    final String story_url = "story_url";
    static String  previewstory = null;
    static String  storyurl = null;
    LinearLayout llseekbar,llfortime;
    RelativeLayout playerLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected_story);

        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        audioPlayerViewObj = new AudioPlayerView(this);
        audioPlayerViewObj.audioSeekBar = (SeekBar) findViewById(R.id.audioSeekBarId);
        audioPlayerViewObj.bufferTextView = (TextView) findViewById(R.id.bufferTextViewId);
        audioPlayerViewObj.bufferText = (TextView) findViewById(R.id.bufferTextView);
        audioPlayerViewObj.playTextView = (TextView) findViewById(R.id.playedTextViewId);
        audioPlayerViewObj.playText = (TextView) findViewById(R.id.playedTextView);

        playPauseAudioBtn = (Button) findViewById(R.id.playAudioBtnId);
        audioPlayerViewObj.playOrPauseBtn = playPauseAudioBtn;
        stopAudioBtn = (Button) findViewById(R.id.stopAudioBtnId);
        storyname = (TextView) findViewById(R.id.storyname);
        bookname = (TextView) findViewById(R.id.bookname);
        duration = (TextView) findViewById(R.id.duration);
        director = (TextView) findViewById(R.id.director);
        authorname = (TextView) findViewById(R.id.authorname);
        artistname = (TextView) findViewById(R.id.artistname);
        authoreimage = (ImageView) findViewById(R.id.authorimage);
        artistimage = (ImageView) findViewById(R.id.artistimage);
        authoreimageinfo = (ImageView) findViewById(R.id.authorimageinfo);
        artistimageinfo = (ImageView) findViewById(R.id.artistimageinfo);
        preview = (Button) findViewById(R.id.preview);
        story = (Button) findViewById(R.id.story);
        llseekbar = (LinearLayout) findViewById(R.id.llseekbar);
        llfortime = (LinearLayout) findViewById(R.id.llfortime);
        playerLayout = (RelativeLayout) findViewById(R.id.playerLayout);

        invisible();
        Intent in = getIntent();
        final StoryListModel StoryListobj = (StoryListModel)in.getSerializableExtra("StoryList");

        final String storyID = StoryListobj.getStoryid();

        if(Utility.isExternalStorageReadable()) {
            String str=Utility.decodeFile( "pointerStroyListJson.txt");
            // fatchMyStoryListJson("pointerStroyListJson.txt", clickedId, IdType);
            try {
                parseMyStoryListJson(str,storyID);
            } catch (JSONException error117) {
                error117.printStackTrace();
                Log.e(HomeActivity.LogTag, "error117" + error117);
            }
        }
        else{
            try {
                fatchMyStoryListJsonFromIM("pointerStroyListJson.txt", storyID);
            } catch (IOException error118) {
                error118.printStackTrace();
                Log.e(HomeActivity.LogTag, "error118" + error118);
            }
        }
        audioPlayerViewObj.SongUrl = previewstory;
        preview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                audioPlayerViewObj.SongUrl = previewstory;
                if(HomeActivity.audioPlayerViewObj.mp != null){

                    Log.d(HomeActivity.LogTag,"\n  HomeActivityaudioPlayerViewObj.mp. : "+ String.valueOf(HomeActivity.audioPlayerViewObj.mp));
                    HomeActivity.audioPlayerViewObj.stopAudioPlayer();
                    Log.d("HomeActivityaudioPlayerViewreleseObj.mp", String.valueOf(HomeActivity.audioPlayerViewObj.mp));
                    audioPlayerViewObj.playOrPauseAudioPlayer();
                    visibleSeekbar();
                }

                    else {

                    if(audioPlayerViewObj.mp != null) {

                        audioPlayerViewObj.stopAudioPlayer();
                        audioPlayerViewObj.bufferTextView.setText("" + 0 + "%");
                        Log.d(HomeActivity.LogTag, "\n  audioPlayerViewObj.SongUrl : " + String.valueOf(audioPlayerViewObj.SongUrl));
                        audioPlayerViewObj.playOrPauseAudioPlayer();
                        visibleSeekbar();
                    }
                    else{
                        audioPlayerViewObj.playOrPauseAudioPlayer();
                        visibleSeekbar();
                    }
                    }
                }


        });
        story.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                audioPlayerViewObj.SongUrl = storyurl;
                if(HomeActivity.audioPlayerViewObj.mp != null){
                    Log.d(HomeActivity.LogTag,"\n  HomeActivityaudioPlayerViewObj.mp. : "+ String.valueOf(HomeActivity.audioPlayerViewObj.mp));
                    HomeActivity.audioPlayerViewObj.stopAudioPlayer();
                    Log.d("HomeActivityaudioPlayerViewreleseObj.mp", String.valueOf(HomeActivity.audioPlayerViewObj.mp));
                    audioPlayerViewObj.playOrPauseAudioPlayer();
                    visibleSeekbar();
                }

                    else {
                    if(audioPlayerViewObj.mp != null) {
                        audioPlayerViewObj.stopAudioPlayer();
                        Log.d(HomeActivity.LogTag, "\n  audioPlayerViewObj.SongUrl : " + String.valueOf(audioPlayerViewObj.SongUrl));
                        audioPlayerViewObj.playOrPauseAudioPlayer();
                        visibleSeekbar();
                    }
                    else{
                        audioPlayerViewObj.playOrPauseAudioPlayer();
                        visibleSeekbar();
                    }
                }
                }

        });


        stopAudioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayerViewObj.stopAudioPlayer();
            }
        });

        playPauseAudioBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioPlayerViewObj.playOrPauseAudioPlayer();
            }
        });

//        Toast.makeText(getApplicationContext(),
//                      "Clicked on : " + StoryListobj.getAuthorname(), Toast.LENGTH_LONG).show();

  /*      storyname.setText(StoryListobj.getStoryname());
        bookname.setText(StoryListobj.getBookname());
        duration.setText(StoryListobj.getTimeduration());
        director.setText(StoryListobj.getDirector());

        authorname.setText(StoryListobj.getAuthorname());
        artistname.setText(StoryListobj.getArtistname());

        final String authorId = StoryListobj.getAuthoreid();
        final String artistid = StoryListobj.getArtistid();
        String authornewid="a"+authorId;
        String artistnewid="a"+artistid;
        Context context = authoreimage.getContext();
        Context context1 = artistimage.getContext();
        // int id = context.getResources().getIdentifier(newid, "drawable", context.getPackageName());
        int id = Utility.getImageById(authornewid, context);
        authoreimage.setImageResource(id);
        int id1 = Utility.getImageById(artistnewid, context1);
        artistimage.setImageResource(id1);
*/

        final String authorId = StoryListobj.getAuthoreid();
        final String artistid = StoryListobj.getArtistid();
        View.OnClickListener infoOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == R.id.authorimageinfo){
                    clickedId =authorId;
                    idType = StoryListActivity.author_point;
                }else if(v.getId() == R.id.artistimageinfo){
                    clickedId =artistid;
                    idType = StoryListActivity.voice_artist;
                }
                Intent intent = new Intent(SelectedStoryActivity.this,InfoActivity.class);
                intent.putExtra("clickedId", clickedId);
                intent.putExtra("IdType", idType);
                startActivity(intent);
            }
        };

        authoreimageinfo.setOnClickListener(infoOnClickListener);
        artistimageinfo.setOnClickListener(infoOnClickListener);



    }


    private void invisible() {

        llseekbar.setVisibility(View.INVISIBLE);
        llfortime.setVisibility(View.INVISIBLE);
        playerLayout.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.bufferTextView.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.bufferText.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.playTextView.setVisibility(View.INVISIBLE);
        audioPlayerViewObj.playText.setVisibility(View.INVISIBLE);
    }


    private void visibleSeekbar() {
        llseekbar.setVisibility(View.VISIBLE);
        llfortime.setVisibility(View.VISIBLE);
        playerLayout.setVisibility(View.VISIBLE);
    }


    private void fatchMyStoryListJsonFromIM(String fileName, String storyID) throws IOException {
        try {

            Log.e(HomeActivity.LogTag, "\n mInternalStorageAvailable: " + "true" + "\n");
            BufferedReader bufferedReader = new BufferedReader(new FileReader(new
                    File(getFilesDir() + File.separator + fileName)));
            String read;
            StringBuilder builder = new StringBuilder("");

            while ((read = bufferedReader.readLine()) != null) {
                builder.append(read);
            }
            String jString = builder.toString();
            Log.d("Output", builder.toString());
            bufferedReader.close();
            parseMyStoryListJson(jString,storyID);


        }catch (Exception error116)
        {
            error116.printStackTrace();
            Log.e(HomeActivity.LogTag, "error116" + error116);
        }


    }


    private void parseMyStoryListJson(String jString,String storyID) throws JSONException {

        JSONArray jObject = new JSONArray(jString);

            //Iterate the jsonArray and print the info of JSONObjects
            for (int i = 0; i < jObject.length(); i++) {

                JSONObject jsonObject = jObject.getJSONObject(i);
                String storyId = jsonObject.optString(StoryListActivity.story_Id);


                if(storyId.equals(storyID)){

                    String storyNameStr = jsonObject.optString(StoryListActivity.story_name);
                    String booknameStr = jsonObject.optString(StoryListActivity.book_name);
                    String directorStr = jsonObject.optString(StoryListActivity.director);
                    String storyduration = jsonObject.optString(StoryListActivity.story_duration);
                     previewstory = jsonObject.optString(preview_story);
                     storyurl = jsonObject.optString(story_url);

                    JSONObject author_pointJsonObject = jsonObject.getJSONObject(StoryListActivity.author_point);
                    String authorNameStr = author_pointJsonObject.optString(StoryListActivity.author_name);
                    String authorId = author_pointJsonObject.optString(StoryListActivity.author_id);

                    JSONObject voice_artistJsonObject = jsonObject.getJSONObject(StoryListActivity.voice_artist);
                    String artistnameStr = voice_artistJsonObject.optString(StoryListActivity.artist_name);
                    String artistId = voice_artistJsonObject.optString(StoryListActivity.artist_id);

                    storyname.setText(storyNameStr);
                    bookname.setText(booknameStr);
                    duration.setText(storyduration);
                    director.setText(directorStr);
                    authorname.setText(authorNameStr);
                    artistname.setText(artistnameStr);

                    String authornewid="a"+authorId;
                    String artistnewid="a"+artistId;
                    Context context = authoreimage.getContext();
                    Context context1 = artistimage.getContext();
                    // int id = context.getResources().getIdentifier(newid, "drawable", context.getPackageName());
                    int id = Utility.getImageById(authornewid, context);
                    authoreimage.setImageResource(id);
                    int id1 = Utility.getImageById(artistnewid, context1);
                    artistimage.setImageResource(id1);

                    audioPlayerViewObj.SongUrl = previewstory;
                    Log.d("previewstory", previewstory);
                    Log.d("storyurl", storyurl);

                }
            }
        }


    public void startAudioStreaming(View v){ audioPlayerViewObj.streamAudioPlayer();}

    @Override
    public void onClick(View v) {
        startAudioStreaming(v);
    }
}

