package com.creo.gostories;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Created by Admin_2 on 22-07-2015.
 */
public class Utility {

    static Boolean sdCardAvailable;
    static FileOutputStream fos;

    private static String algorithm = "AES";
    static SecretKey yourKey = null;
    public Bitmap getOptimizedImageFile(File f){

        try {

            //Decode image size
            BitmapFactory.Options o1 = new BitmapFactory.Options();
            o1.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o1);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.

            // Set width/height of recreated image
            final int REQUIRED_SIZE=85;

            int width_tmp=o1.outWidth, height_tmp=o1.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with current scale values
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            System.out.print("in utility"+bitmap);
            return bitmap;

        } catch (FileNotFoundException error119) {
            Log.e(HomeActivity.LogTag, "error119" + error119);
        }
        catch (IOException error120) {
            error120.printStackTrace();
            Log.e(HomeActivity.LogTag, "error120" + error120);
        }
        return null;
    }

    public static int getImageById(String authorId, Context context) {
        int id = context.getResources().getIdentifier(authorId, "drawable", context.getPackageName());
        return id;
    }


    public static File createFileUnderGoStoriesDir(String fileName)
    {
        File storyListLocalJSONFile = null;
        try{
            File GoStoriesDir = new File(Environment.getExternalStorageDirectory().getPath()+ File.separator+"GoStories");
            if (!GoStoriesDir.exists()) {
                if(GoStoriesDir.mkdir()){
                    Log.e(HomeActivity.LogTag, "\n GoStories directory creation SUCCESS \n");
                }else {
                    Log.e(HomeActivity.LogTag, "\n GoStories directory creation FAILED \n");
                }
            }
            storyListLocalJSONFile = new File(GoStoriesDir+ File.separator+fileName);

            if(storyListLocalJSONFile.exists()){
                Log.e(HomeActivity.LogTag, "\n storyListLocalJSONFile created \n");
            }else {
                if (storyListLocalJSONFile.createNewFile()) {
                    Log.e(HomeActivity.LogTag, "\n storyListLocalJSONFile creation SUCCESS \n");
                }else {
                    Log.e(HomeActivity.LogTag, "\n storyListLocalJSONFile creation FAILED \n");
                }
            }
        }catch (Exception error121){
            Log.e(HomeActivity.LogTag, "error121" + error121);

        }
        return storyListLocalJSONFile;
    }




    public static void saveStringOnSDCardUnderFile(String strJson, String filename) {

        try{
            String fileName = filename;
            File reqFile = createFileUnderGoStoriesDir(fileName);

            if (reqFile != null){
                FileWriter writer = new FileWriter(reqFile.getAbsolutePath());
                writer.append(strJson);
                writer.flush();
                writer.close();
            }else {
                Log.e(HomeActivity.LogTag, "Could not save file on sd");
            }
            // JSONObject tempObj = new JSONObject(strJson);
            // String tmpStr = tempObj.toString(3);
            // Log.e(MainActivity.LogTag, strJson);

        }catch (Exception error122){

            String errStr = "\n\nError in Saving JsonString to file: " + error122 +"\n\n";
            Log.e(HomeActivity.LogTag, errStr);
            Log.e(HomeActivity.LogTag, "error122" + error122);

        }

    }



    public static boolean isExternalStorageAvailable() {

        String state = Environment.getExternalStorageState();
        boolean mExternalStorageAvailable = false;
        boolean mExternalStorageWriteable = false;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");

        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");
            Log.e(HomeActivity.LogTag, "\n mExternalStorageWriteable: " + "false"+"\n");

        } else {
            // Something else is wrong. It may be one of many other states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "false"+"\n");
        }

        if (mExternalStorageAvailable == true
                && mExternalStorageWriteable == true) {
            Log.e(HomeActivity.LogTag, "\n mExternalStorageAvailable: " + "true"+"\n");
            return true;
        } else {
            return false;
        }
    }


    public static void saveDataOnPhysicalMemory(String strJson, String fileName, Context context) {


        sdCardAvailable = isExternalStorageWritable();



        if(sdCardAvailable)
        {
            Log.e(HomeActivity.LogTag, "\n external storage: " +  "\n");
           // saveStringOnSDCardUnderFile(strJson, fileName);
            saveFile(strJson, fileName);
        }
        else{


            try {
                fos = context.openFileOutput("pointerStroyListJson.txt", Context.MODE_PRIVATE);

                //default mode is PRIVATE, can be APPEND etc.
                fos.write(strJson.getBytes());
                fos.close();
                Log.e(HomeActivity.LogTag, "\n SUCCESS internal storage: " +  "\n");
            } catch (FileNotFoundException error123) {
                error123.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n FAILED internal storage: " + error123.getLocalizedMessage()+ "\n");


            }
            catch (IOException error124) {
                error124.printStackTrace();
                Log.e(HomeActivity.LogTag, "\n FAILED internal storage: " + error124.getLocalizedMessage()+ "\n");
            }
        }



    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }



    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /* Checks if external storage is available for read and write */
    public static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            Log.e(HomeActivity.LogTag, "\n external storage: true" +  "\n");
            return true;


        }
        Log.e(HomeActivity.LogTag, "\n external storage:false " +  "\n");
        return false;
    }

    /* Checks if external storage is available to at least read */
    public static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }


   static void saveFile(String stringToSave,String encryptedFileName) {
        try {
            String fileName = encryptedFileName;
            File reqFile = createFileUnderGoStoriesDir(fileName);

            File file = new File(Environment.getExternalStorageDirectory()
                    + File.separator, encryptedFileName);
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(reqFile));
            yourKey = generateKey();
            byte[] filesBytes = encodeFile(yourKey, stringToSave.getBytes());
            bos.write(filesBytes);
            bos.flush();
            bos.close();
        } catch (FileNotFoundException error125) {
            error125.printStackTrace();
            Log.e(HomeActivity.LogTag, "error125" + error125);
        } catch (IOException error126) {
            error126.printStackTrace();
            Log.e(HomeActivity.LogTag, "error126" + error126);
        } catch (Exception error127) {
            error127.printStackTrace();
            Log.e(HomeActivity.LogTag, "error127" + error127);
        }
    }



    public static SecretKey generateKey(char[] passphraseOrPin, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        // Number of PBKDF2 hardening rounds to use. Larger values increase
        // computation time. You should select a value that causes computation
        // to take >100ms.
        final int iterations = 1000;

        // Generate a 256-bit key
        final int outputKeyLength = 256;

        SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec keySpec = new PBEKeySpec(passphraseOrPin, salt, iterations, outputKeyLength);
        SecretKey secretKey = secretKeyFactory.generateSecret(keySpec);
        return secretKey;
    }

    public static SecretKey generateKey() throws NoSuchAlgorithmException {
        // Generate a 256-bit key
        final int outputKeyLength = 256;
        SecureRandom secureRandom = new SecureRandom();
        // Do *not* seed secureRandom! Automatically seeded from system entropy.
        KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
        keyGenerator.init(outputKeyLength, secureRandom);
        yourKey = keyGenerator.generateKey();
        return yourKey;
    }

    public static byte[] encodeFile(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] encrypted = null;
        byte[] data = yourKey.getEncoded();
        SecretKeySpec skeySpec = new SecretKeySpec(data, 0, data.length,
                algorithm);
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(
                new byte[cipher.getBlockSize()]));
        encrypted = cipher.doFinal(fileData);
        return encrypted;
    }


    public static byte[] decodeFile(SecretKey yourKey, byte[] fileData)
            throws Exception {
        byte[] decrypted = null;
        Cipher cipher = Cipher.getInstance(algorithm);
        cipher.init(Cipher.DECRYPT_MODE, yourKey, new IvParameterSpec(
                new byte[cipher.getBlockSize()]));
        decrypted = cipher.doFinal(fileData);
        return decrypted;
    }
    static String decodeFile(String encryptedFileName) {

        String str = null;
        try {
            byte[] decodedData = decodeFile(yourKey, readFile(encryptedFileName));
            str = new String(decodedData);
            System.out.println("DECODED FILE CONTENTS : " + str);
        } catch (Exception error128) {
            error128.printStackTrace();
            Log.e(HomeActivity.LogTag, "error128" + error128);
        }
        return str;
    }

    public static byte[] readFile(String encryptedFileName) {
        byte[] contents = null;

        File file = new File(Environment.getExternalStorageDirectory()
                + File.separator+ "GoStories", encryptedFileName);
        int size = (int) file.length();
        contents = new byte[size];
        try {
            BufferedInputStream buf = new BufferedInputStream(
                    new FileInputStream(file));
            try {
                buf.read(contents);
                buf.close();
            } catch (IOException error129) {
                error129.printStackTrace();
                Log.e(HomeActivity.LogTag, "error129" + error129);
            }
        } catch (FileNotFoundException error130) {
            error130.printStackTrace();
            Log.e(HomeActivity.LogTag, "error130" + error130);
        }
        return contents;
    }

}
