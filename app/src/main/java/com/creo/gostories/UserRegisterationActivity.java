package com.creo.gostories;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Admin_2 on 13-08-2015.
 */
public class UserRegisterationActivity extends Activity implements View.OnClickListener {

    EditText emailID;
    EditText mobileNumber;
    Button register;
    Context context = this;
    String success = "Success";
    String error = "Error";
    Boolean success_response = false;
    Boolean error_response = false;
    String success_msg = "Congratulations you have successfully registered and your wallet creadited 100";
    String currency_type = "";
    String not_connected = "You are not connected";
    String connected = "You are connected";
     String url = "http://gostories.co.in/Sandeep/GoStoriesHost/PHP/newRegistration.php";
  //  String url = "http://192.168.1.14/GoStoriesHost/PHP/newRegistration.php";
    ProgressDialog pd;
    String WALLET_BALANCE = "WALLET_BALANCE";
    String SELF_PROMO_CODE = "SELF_PROMO_CODE";
    Registration registrationNew = null;
    String gss_error301 = "GSS Error-301 : Contact GoStories support team with this error code.friendz@gostories.co.in";
    String gss_error302 = "GSS Error-302 : Contact GoStories support team with this error code.friendz@gostories.co.in";
    Boolean cancel = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registeration_activity);


        emailID = (EditText) findViewById(R.id.editTextEmailID);
        mobileNumber = (EditText) findViewById(R.id.editMoblieNumber);
        register = (Button) findViewById(R.id.register);

        String emailString = getEmail(context);
        Log.e(HomeActivity.LogTag, emailString);
        emailID.setText(emailString);

        register.setOnClickListener(this);
    }


    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);
        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        } return account;
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.register:

                String email = emailID.getText().toString();
                String mobileNo = mobileNumber.getText().toString();

//                if (mobileNo.length()>0 && mobileNo.length()< 10) {
//
//                    Toast.makeText(getApplicationContext(), "Please Enter Valid Mobile Number", Toast.LENGTH_SHORT).show();
//                }else

                if (email.equals("")) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    // set title
                    alertDialogBuilder.setTitle("Please Enter Email ID");
                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    //  Toast.makeText(getApplicationContext(), "Please Enter Email ID", Toast.LENGTH_SHORT).show();
                }

                else if (!isValidEmail(email)) {
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    // set title
                    alertDialogBuilder.setTitle("Please Enter valid Email ID");
                    // set dialog message
                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog,int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();

                    //Toast.makeText(getApplicationContext(), "Please Enter valid Email ID", Toast.LENGTH_SHORT).show();
                }

                else {
//                    Intent i_register = new Intent(UserRegisterationActivity.this, HomeActivity.class);
//                    startActivity(i_register);
//                    finish();
//                    SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
//                    editor.putBoolean("key_name1", true);
//                    editor.commit();
                    if(isConnected()){
                        Log.e(HomeActivity.LogTag, connected);
                        pd = new ProgressDialog(UserRegisterationActivity.this);
                        pd.setMessage("Uploading info...");
                        pd.setCancelable(false);
                        pd.show();
                        new HttpAsyncTask().execute(url);
                    }

                    else{
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder.setTitle(not_connected);
                        // set dialog message
                        alertDialogBuilder
                                .setCancelable(false)
                                .setNegativeButton("Cancel",new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // if this button is clicked, just close
                                        // the dialog box and do nothing
                                        dialog.cancel();
                                    }
                                });
                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();
                        // Toast.makeText(getApplicationContext(), not_connected, Toast.LENGTH_SHORT).show();
                        Log.e(HomeActivity.LogTag, not_connected);
                    }
                }
                break;

        }
    }

    private class HttpAsyncTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            TelephonyManager TelephonyMgr = (TelephonyManager)getSystemService(TELEPHONY_SERVICE);
            String m_deviceId = TelephonyMgr.getDeviceId();
            //Toast.makeText(getApplicationContext(),m_deviceId , Toast.LENGTH_SHORT).show();
            Log.e(HomeActivity.LogTag, m_deviceId);
            Registration registration = new Registration();
            registration.setEmailID(emailID.getText().toString());
            registration.setMobileNumber(mobileNumber.getText().toString());
            registration.setUdidNumber(m_deviceId);
            return POST(urls[0],registration);
        }
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {

            TextView myMsg = new TextView(UserRegisterationActivity.this);
            myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
            myMsg.setTextSize(20);
            pd.dismiss();
            if(registrationNew!= null) {
                String walletBalance = registrationNew.getWalletBalance();
                String error = registrationNew.getError();
                String errorKey = registrationNew.getErrorKey();
                Log.d(HomeActivity.LogTag, "\n  WalletBalance. : " + walletBalance);
                Log.d(HomeActivity.LogTag, "\n  error. : " + error);

                    if (success_response) {

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        // set title
                        myMsg.setText(success);
                      //  alertDialogBuilder.setTitle(success);
                        String final_msg = success_msg + currency_type;
                        alertDialogBuilder.setCustomTitle(myMsg);
                        // set dialog message
                        alertDialogBuilder.setMessage(final_msg);
                        alertDialogBuilder
                                .setCancelable(false)
                                .setNegativeButton("Start Enjoying", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {
                                        dialog.cancel();
                                        cancel = true;
                                        afterCancel();
                                    }

                                    private void afterCancel() {

                                        if(cancel){
                                            SharedPreferences.Editor editor = getSharedPreferences("MY_PREFS_NAME", MODE_PRIVATE).edit();
                                            String promoCode = registrationNew.getPromoCode();
                                            String totalPoint = registrationNew.getTotalPoint();
                                            editor.putString("email", emailID.getText().toString());
                                            editor.putString("promo_code",promoCode);
                                            editor.putString("toal_point",totalPoint);
                                            editor.commit();

                                            Intent i_register = new Intent(UserRegisterationActivity.this, HomeActivity.class);
                                            startActivity(i_register);
                                            finish();
                                        }
                                    }
                                });

                        // create alert dialog
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        // show it
                        alertDialog.show();

                        // Toast.makeText(getBaseContext(), result, Toast.LENGTH_LONG).show();
                    }

                else if(error_response) {

                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    // set title
                    myMsg.setText(errorKey);
                    alertDialogBuilder.setTitle(errorKey);
                    alertDialogBuilder.setCustomTitle(myMsg);
                    // set dialog message
                    alertDialogBuilder.setMessage(error);
                    alertDialogBuilder
                            .setCancelable(false)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    // if this button is clicked, just close
                                    // the dialog box and do nothing
                                    dialog.cancel();
                                }
                            });
                    // create alert dialog
                    AlertDialog alertDialog = alertDialogBuilder.create();
                    // show it
                    alertDialog.show();
                    //  Toast.makeText(getApplicationContext(), result, Toast.LENGTH_SHORT).show();
                    Log.e(HomeActivity.LogTag, "result" + result);
                }
            }
            else{

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                // set title

                myMsg.setText("error 131");
                alertDialogBuilder.setCustomTitle(myMsg);
                // set dialog message
                alertDialogBuilder.setMessage(gss_error301);
                alertDialogBuilder
                        .setCancelable(false)
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();

            }


        }
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public boolean isConnected(){
        ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Activity.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    public String POST(String url,Registration registration){
        InputStream inputStream = null;
        String result = "";
        try {

            HttpClient httpclient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            String json = "";
            JSONObject jsonObject = new JSONObject();
            jsonObject.accumulate("GivenPhoneNumber", registration.getMobileNumber());
            jsonObject.accumulate("GivenEmailId", registration.getEmailID());
            jsonObject.accumulate("GivenUDId", registration.getUdidNumber());
            json = jsonObject.toString();

            StringEntity se = new StringEntity(json);
            httpPost.setEntity(se);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");
            HttpResponse httpResponse = httpclient.execute(httpPost);
            inputStream = httpResponse.getEntity().getContent();
            if(inputStream != null) {
                result = convertInputStreamToString(inputStream);
                registrationNew = parseMyStoryListJson(result);
            }
            else
                result = "Not Register";

        } catch (Exception error131) {

            result = error131.getLocalizedMessage();
            Log.d(HomeActivity.LogTag,"error131 :"+ result);
        }

        return result;
    }

    public Registration parseMyStoryListJson(String strJson) {
        Registration registration = new Registration();

        TextView myMsg = new TextView(UserRegisterationActivity.this);
        myMsg.setGravity(Gravity.CENTER_HORIZONTAL);
        myMsg.setTextSize(20);
        try {

           JSONObject jsonObject = new JSONObject(strJson);
           if(jsonObject.has(success)) {
               JSONObject successObj = jsonObject.getJSONObject(success);
               String promoCode = successObj.optString(SELF_PROMO_CODE);
               String walletBalance = successObj.optString(WALLET_BALANCE);
               Log.d(HomeActivity.LogTag, "\n  promoCode. : " + promoCode);
               Log.d(HomeActivity.LogTag, "\n  walletBalance. : " + walletBalance);
               registration.setPromoCode(promoCode);
               registration.setWalletBalance(walletBalance);
               registration.setTotalPoint("1000");
               success_response = true;

           }
            if(jsonObject.has(error)) {

                JSONObject errorObj = jsonObject.getJSONObject(error);
//                for(int i = 0;i<errorObj.length();i++) {//
//                    String error = errorObj.toString(i);
//                    Log.d(HomeActivity.LogTag, "\n  error. in loop : " + error);
                Iterator keys = errorObj.keys();
                String currentDynamicKey = (String)keys.next();
                Log.d(HomeActivity.LogTag, "\n  currentDynamicKey. : " + currentDynamicKey);
                // get the value of the dynamic key
                String currentDynamicValue = errorObj.optString(currentDynamicKey);
              //  JSONObject currentDynamicValue = errorObj.getJSONObject(currentDynamicKey);
                Log.d(HomeActivity.LogTag, "\n  currentDynamicValue. : " + currentDynamicValue);
                registration.setError(currentDynamicValue);
                registration.setErrorKey(currentDynamicKey);
                error_response = true;
                // Log.d(HomeActivity.LogTag, "\n  error. : " + errorStr);
           /*     while(keys.hasNext()) {
                        // loop to get the dynamic key
                        String currentDynamicKey = (String)keys.next();
                        Log.d(HomeActivity.LogTag, "\n  currentDynamicKey. : " + currentDynamicKey);
                        // get the value of the dynamic key
                        JSONObject currentDynamicValue = errorObj.getJSONObject(currentDynamicKey);
                        Log.d(HomeActivity.LogTag, "\n  currentDynamicValue. : " + currentDynamicValue);
                        // do something here with the value...
                    }
                    if(!error.equals("")) {

                       // String errorStr = errorObj.;
                        //registration.setError(errorStr);
                       // Log.d(HomeActivity.LogTag, "\n  error. : " + errorStr);
                    }
     /*               String error1 = errorObj.optString(gssError1);
                    String error2 = errorObj.optString(gssError2);
                    String error3 = errorObj.optString(gssError3);
                    String error4 = errorObj.optString(gssError4);

                    Log.d(HomeActivity.LogTag, "\n  error1. : " + error1);
                    Log.d(HomeActivity.LogTag, "\n  error2. : " + error2);
                    Log.d(HomeActivity.LogTag, "\n  error3. : " + error3);
                    Log.d(HomeActivity.LogTag, "\n  error4. : " + error4);

                    if (!error1.equals("")) {
                        registration.setError(error1);
                        Log.d(HomeActivity.LogTag, "\n  error1. : " + error1);
                    }
                    if (!error2.equals("")) {
                        registration.setError(error2);
                        Log.d(HomeActivity.LogTag, "\n  error2. : " + error2);
                    }
                    if (!error3.equals("")) {
                        registration.setError(error3);
                        Log.d(HomeActivity.LogTag, "\n  error3. : " + error3);
                    }
                    if (!error4.equals("")) {
                        registration.setError(error4);
                        Log.d(HomeActivity.LogTag, "\n  error4. : " + error4);
                    }*/
               // }
            }
        }
        catch (JSONException error132) {
            String errStr = "Error in parseMyJson in StoryList: " + error132.getMessage();
            Log.d(HomeActivity.LogTag,"\n  error132. : "+ error132);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
            // set title

            myMsg.setText("error 132");
            alertDialogBuilder.setCustomTitle(myMsg);
            // set dialog message
            alertDialogBuilder.setMessage(gss_error302);
            alertDialogBuilder
                    .setCancelable(false)
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            // if this button is clicked, just close
                            // the dialog box and do nothing
                            dialog.cancel();
                        }
                    });
            // create alert dialog
            AlertDialog alertDialog = alertDialogBuilder.create();
            // show it
            alertDialog.show();
        }
        return registration;
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException {
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;
        inputStream.close();
        return result;
    }
}
